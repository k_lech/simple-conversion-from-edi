/*
Program do odczytu zakodowanego pliku (.txt) skladajacego sie z układu TAGÓW, tj:
POCZATEK KOMUNIKATU							(Faktura handlowa / Paragon; Nr Referenyjny (o dowolnej długości); Wersja dokumentu: duplikat / oryginał / kopia)
PLATNOSCI									(Sposób płatności: gotówka / karta kredytowa / przelew krajowy / przelew zagraniczny; Kwota; Waluta)
IDENTYFIKATOR CZASU/DATY					(Data: wystawienia faktury / płatnosci / dostawy; Data w formacie wybranym przez użytkownika (RRRRMMDD / DDMMRRRR)
IDENTYFIKATOR NABYWCY I DOSTAWCY			(Nazwa nabywcy / adres / nip / miasto / kod pocztowy)
LISTA POZYCJI TOWAROWYCH (do 6 pozycji)		(Nr pozycji / Nazwa towaru / ilość / cena jednostkowa / cena całkowita)
OSOBA WYSTAWIAJACA DOKUMENT					(Imie i nazwisko)

Program wykonał: Krystian Lech
*/
#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include "ODCZYT_DANYCH.h"
using namespace std;
int main()
{
	ODCZYT_DANYCH nowy_ODCZYT;
	nowy_ODCZYT.GETwiersz_tekstu();
	nowy_ODCZYT.getPodzial_na_segmenty();
	nowy_ODCZYT.getBGM();
	nowy_ODCZYT.getPAI();
	nowy_ODCZYT.getCOM_AB();
	nowy_ODCZYT.getCOM_BB();
	nowy_ODCZYT.getDTM();
	nowy_ODCZYT.getCTA();
	nowy_ODCZYT.getSPRAWDZ_LIN();
	nowy_ODCZYT.getKOMUNIKAT_KONCOWY();

	system("pause");
	return 0;
}

