#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

#pragma once
class ODCZYT_DANYCH
{
private:
	int liczba_wierszy , pozycje_transportowe;
	string wiersz, BGM, DTM, COM_AB, COM_BB, CTA, PAI;
	string LIN_1, LIN_2, LIN_3, LIN_4, LIN_5, LIN_6;
	string kod_dokumentu, NR_REF, wersja_dokumentu;							//BGM
	string sposob_platnosci, waluta, kwota;									//PAI
	string nazwa_nabywcy, nip_n, miasto_n, kod_pocztowy_n;					//COM_AB
	string nazwa_odbiorcy, nip_o, miasto_o, kod_pocztowy_o;					//COM_AB
	string data_wystawienia_dokumentu, KOD_DATY,  format;					//DTM
	string wystawiajacy;													//CTM
	string nr_pozycji, nazwa_towaru, ilosc, cena_jednostkowa, cena_razem;	//LIN
	vector <string> wiersz_tekstu;
public:
	ODCZYT_DANYCH();

	vector <string> GETwiersz_tekstu();
	
	void  setPodzial_na_segmenty(string BGM, string DTM, string COM_AB, string COM_BB, string CTA);
	void  setBGM(string kod_dokumentu, string NR_REF, string wersja_dokumentu);
	void  setPAI(string sposob_platnosci, string kwota, string waluta);
	void  setCOM_AB(string nazwa_nabywcy, string nip_n, string miasto_n, string kod_pocztowy_n);
	void  setCOM_BB(string nazwa_odbiorcy, string nip_o, string miasto_o, string kod_pocztowy_o);
	void  setDTM(string data_wystawienia_dokumentu,string KOD_DATY, string format);
	void  setCTA(string wystawiajacy);
	void  setLIN(string nr_pozycji, string nazwa_towaru, string ilosc, string cena_jednostkowa, string cena_razem);
	void  setSPRAWDZ_LIN(int pozycje_transportowe);


	void  getPodzial_na_segmenty();
	void  getBGM();
	void  getPAI();
	void  getCOM_AB();
	void  getCOM_BB();
	void  getDTM();
	void  getCTA();
	void  getSPRAWDZ_LIN();
	void  getLIN();
	void  getKOMUNIKAT_KONCOWY();
	





};

