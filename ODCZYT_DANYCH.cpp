#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "ODCZYT_DANYCH.h"



ODCZYT_DANYCH::ODCZYT_DANYCH()
{
}
std::vector<std::string> ODCZYT_DANYCH::GETwiersz_tekstu()	//Wczytaj_zdania
{
	wiersz_tekstu.resize(liczba_wierszy);
	int nr_wiersza = 0;
	string LIN_1, LIN_2, LIN_3, LIN_4, BGM;
	ifstream plik("RAPORT.txt", std::ios::in);		//lokalizacja pliku
	if (plik.good() == true)
	{
		while (!plik.eof())
		{

			getline(plik, wiersz);
			nr_wiersza++;
			liczba_wierszy = +nr_wiersza;
			wiersz_tekstu.push_back(wiersz);
		}
	}
	else cout << "ERROR\n\n";
	return wiersz_tekstu;
}

void  ODCZYT_DANYCH::setPodzial_na_segmenty(string BGM, string DTM, string COM_AB, string COM_BB, string CTA)
{
	this->BGM = BGM;
	this->DTM = DTM;
	this->COM_AB = COM_AB;
	this->COM_BB = COM_BB;
	this->CTA = CTA;
}
void  ODCZYT_DANYCH::setBGM(string kod_dokumentu, string NR_REF, string wersja_dokumentu)
{
	this->kod_dokumentu = kod_dokumentu;
	this->NR_REF = NR_REF;
	this->wersja_dokumentu = wersja_dokumentu;
}
void  ODCZYT_DANYCH::setPAI(string sposob_platnosci, string kwota, string waluta)
{
	this->sposob_platnosci = sposob_platnosci;
	this->kwota = kwota;
	this->waluta = waluta;
}
void  ODCZYT_DANYCH::setCOM_AB(string nazwa_nabywcy, string nip_n, string miasto_n, string kod_pocztowy_n)
{
	this->nazwa_nabywcy = nazwa_nabywcy;
	this->nip_n = nip_n;
	this->miasto_n = miasto_n;
	this->kod_pocztowy_n = kod_pocztowy_n;
}
void  ODCZYT_DANYCH::setCOM_BB(string nazwa_odbiorcy, string nip_o, string miasto_o, string kod_pocztowy_o)
{
	this->nazwa_odbiorcy = nazwa_odbiorcy;
	this->nip_o = nip_o;
	this->miasto_o = miasto_o;
	this->kod_pocztowy_o = kod_pocztowy_o;
}
void  ODCZYT_DANYCH::setDTM(string data_wystawienia_dokumentu, string KOD_DATY, string format)
{
	this->data_wystawienia_dokumentu = data_wystawienia_dokumentu;
	this->format = format;
}
void  ODCZYT_DANYCH::setCTA(string wystawiajacy)
{
	this->wystawiajacy = wystawiajacy;
}
void  ODCZYT_DANYCH::setSPRAWDZ_LIN(int pozycje_transportowe)
{
	this->pozycje_transportowe = pozycje_transportowe;
}
void  ODCZYT_DANYCH::setLIN(string nr_pozycji, string nazwa_towaru, string ilosc, string cena_jednostkowa, string cena_razem)
{
	this->nr_pozycji = nr_pozycji;
	this->nazwa_towaru = nazwa_towaru;
	this->ilosc = ilosc;
	this->cena_jednostkowa = cena_jednostkowa;
	this->cena_razem = cena_razem;
}



void  ODCZYT_DANYCH::getPodzial_na_segmenty()
{
	for (int i = 0; i < liczba_wierszy; i++)
	{
		size_t found1 = wiersz_tekstu[i].find("BGM");
		if (found1 != std::string::npos)
		{
			BGM = wiersz_tekstu[i];
		}
		size_t found2 = wiersz_tekstu[i].find("DTM");
		if (found2 != std::string::npos)
		{
			DTM = wiersz_tekstu[i];
			//cout << DTM << endl;
		}
		size_t found3 = wiersz_tekstu[i].find("COM+AB");
		if (found3 != std::string::npos)
		{
			//cout << "Znaleziono segment COM+AB w tekscie, w wierszu nr:" << i << endl;
			COM_AB = wiersz_tekstu[i];
			//cout << COM_AB << endl;
		}

		size_t found4 = wiersz_tekstu[i].find("COM+BB");
		if (found4 != std::string::npos)
		{
			//cout << "Znaleziono segment COM+BB w tekscie, w wierszu nr:" << i << endl;
			COM_BB = wiersz_tekstu[i];
			//cout << COM_BB << endl;
		}

		size_t found5 = wiersz_tekstu[i].find("CTA");
		if (found5 != std::string::npos)
		{
			//cout << "Znaleziono segment CTA w tekscie, w wierszu nr:" << i << endl;
			CTA = wiersz_tekstu[i];
			//cout << CTA << endl;
		}
		size_t found6 = wiersz_tekstu[i].find("PAI");
		if (found6 != std::string::npos)
		{
			//cout << "Znaleziono segment PAI w tekscie, w wierszu nr:" << i << endl;
			PAI = wiersz_tekstu[i];
		}
		size_t found7 = wiersz_tekstu[i].find("LIN+1");
		if (found7 != std::string::npos)
		{
			//cout << "Znaleziono segment LIN+1 w tekscie, w wierszu nr:" << i << endl;
			LIN_1 = wiersz_tekstu[i];
		}
		size_t found8 = wiersz_tekstu[i].find("LIN+2");
		if (found8 != std::string::npos)
		{
			//cout << "Znaleziono segment LIN+2 w tekscie, w wierszu nr:" << i << endl;
			LIN_2 = wiersz_tekstu[i];
		}
		size_t found9 = wiersz_tekstu[i].find("LIN+3");
		if (found9 != std::string::npos)
		{
			//cout << "Znaleziono segment LIN+3 w tekscie, w wierszu nr:" << i << endl;
			LIN_3 = wiersz_tekstu[i];
		}
		size_t found10 = wiersz_tekstu[i].find("LIN+4");
		if (found10 != std::string::npos)
		{
			//cout << "Znaleziono segment LIN+4 w tekscie, w wierszu nr:" << i << endl;
			LIN_4 = wiersz_tekstu[i];
		}
		size_t found11 = wiersz_tekstu[i].find("LIN+5");
		if (found11 != std::string::npos)
		{
			//cout << "Znaleziono segment LIN+5 w tekscie, w wierszu nr:" << i << endl;
			LIN_5 = wiersz_tekstu[i];
		}
		size_t found12 = wiersz_tekstu[i].find("LIN+6");
		if (found12 != std::string::npos)
		{
			//cout << "Znaleziono segment LIN+6 w tekscie, w wierszu nr:" << i << endl;
			LIN_6 = wiersz_tekstu[i];
		}
	}
}
void  ODCZYT_DANYCH::getBGM()
{
	//KOD DOKUMENTU
	size_t KOD1 = BGM.find("BGM+380+");
	if (KOD1 != std::string::npos)
	{
		kod_dokumentu = "FAKTURA HANDLOWA";
	}
	size_t KOD2 = BGM.find("BGM+372+");
	if (KOD2 != std::string::npos)
	{
		kod_dokumentu = "PARAGON";
	}
	else if (KOD1 == std::string::npos && KOD2 == std::string::npos)
	{
		cout << "W segmencie BGM nie znaleziono KODU DOKUMENTU.\nBrak uruchomienia sekwencji nr 1 - Poczatek komunikatu." << endl;
	}
	//NR REFERENYJNY
	NR_REF = BGM;
	NR_REF.erase(0, 8);
	size_t KOD3 = NR_REF.find("+");
	if (KOD3 != std::string::npos)
	{
		NR_REF.erase(6);
	}

	//WERSJA DOKUMENTU
	size_t KOD4 = BGM.find("+7'");
	if (KOD4 != std::string::npos)
	{
		//cout << "Znaleziono w segmencie BGM: DUPLIKAT" << endl;
		wersja_dokumentu = "DUPLIKAT";
		//cout << wersja_dokumentu << endl;
	}
	size_t KOD5 = BGM.find("+9'");
	if (KOD5 != std::string::npos)
	{
		//cout << "Znaleziono w segmencie BGM: ORYGINAL" << endl;
		wersja_dokumentu = "ORYGINAL";
		//cout << wersja_dokumentu << endl;
	}
	size_t KOD6 = BGM.find("+31'");
	if (KOD6 != std::string::npos)
	{
		//cout << "Znaleziono w segmencie BGM: KOPIA" << endl;
		wersja_dokumentu = "KOPIA";
		//cout << wersja_dokumentu << endl;
	}
}
void  ODCZYT_DANYCH::getPAI()
{
	//SPOSOB PLATNOSCI
	size_t KOD1 = PAI.find("PAI:10");
	if (KOD1 != std::string::npos)
	{
		//cout << "Sposob platnosci PAI: GOTOWKA" << endl;
		sposob_platnosci = "GOTOWKA";
		//cout << sposob_platnosci << endl;
	}
	size_t KOD2 = PAI.find("PAI:11");
	if (KOD2 != std::string::npos)
	{
		//cout << "Sposob platnosci PAI: KARTA KREDYTOWA" << endl;
		sposob_platnosci = "KARTA KREDYTOWA";
		//cout << sposob_platnosci << endl;
	}
	size_t KOD3 = PAI.find("PAI:12");
	if (KOD3 != std::string::npos)
	{
		//cout << "Sposob platnosci PAI: PRZELEW KRAJOWY" << endl;
		sposob_platnosci = "PRZELEW KRAJOWY";
		//cout << sposob_platnosci << endl;
	}
	size_t KOD4 = PAI.find("PAI:13");
	if (KOD4 != std::string::npos)
	{
		//cout << "Sposob platnosci PAI: PRZELEW ZAGRANICZNY" << endl;
		sposob_platnosci = "PRZELEW ZAGRANICZNY";
		//cout << sposob_platnosci << endl;
	}
	
	//KWOTA
	kwota = PAI;
	kwota.erase(0, 7);
	size_t KOD5 = kwota.find("+");
	if (KOD5 != std::string::npos)
	{
		kwota.erase(KOD5);
		//cout << kwota << endl;
	}
	
	//WALUTA
	size_t KOD6 = PAI.find("EUR");
	if (KOD6 != std::string::npos)
	{
		//cout << "WALUTA PAI: EURO" << endl;
		waluta = "EURO";
		//cout << waluta << endl;
	}
	size_t KOD7 = PAI.find("ZL");
	if (KOD7 != std::string::npos)
	{
		//cout << "WALUTA PAI: Z�OTY" << endl;
		waluta = "ZLOTY";
		//cout << waluta << endl;
	}
	size_t KOD8 = PAI.find("GBP");
	if (KOD8 != std::string::npos)
	{
		//cout << "WALUTA PAI: FUNT BRYTYJSKI" << endl;
		waluta = "FUNT BRYTYJSKI";
		//cout << waluta << endl;
	}
	size_t KOD9 = PAI.find("CZK");
	if (KOD9 != std::string::npos)
	{
		//cout << "WALUTA PAI: KORONY CZESKIE" << endl;
		waluta = "KORONY CZESKIE";
		//cout << waluta << endl;
	}
	size_t KOD10 = PAI.find("CHF");
	if (KOD10 != std::string::npos)
	{
		//cout << "WALUTA PAI: FUNT SZWAJCARSKI" << endl;
		waluta = "FUNT SZWAJCARSKI";
		//cout << waluta << endl;
	}
}
void  ODCZYT_DANYCH::getCOM_AB()
{
	//NAZWA NABYWCY
	nazwa_nabywcy = COM_AB;
	nazwa_nabywcy.erase(0, 7);
	size_t KOD1 = nazwa_nabywcy.find("+");
	if (KOD1 != std::string::npos)
	{
		nazwa_nabywcy.erase(KOD1);
		//cout << nazwa_nabywcy << endl;
	}
	//NIP
	nip_n = COM_AB;
	nip_n.erase(0, 7);
	size_t KOD2 = nip_n.find("+");
	if (KOD2 != std::string::npos)
	{
		nip_n.erase(0, KOD2);
	}
	nip_n.erase(0, 4);
	size_t KOD3 = nip_n.find("+");
	if (KOD3 != std::string::npos)
	{
		nip_n.erase(KOD3);
		//cout << nip_n << endl;
	}
	
	//MIASTO
	miasto_n = COM_AB;
	size_t KOD4 = miasto_n.find("+MT");
	if (KOD4 != std::string::npos)
	{
		miasto_n.erase(0, KOD4);
	}
	miasto_n.erase(0, 4);
	size_t KOD5 = miasto_n.find("+");
	if (KOD5 != std::string::npos)
	{
		miasto_n.erase(KOD5);
		//cout << miasto_n << endl;
	}

	//KOD POCZTOWY
	kod_pocztowy_n = COM_AB;
	kod_pocztowy_n.erase(0, 4);
	size_t KOD6 = kod_pocztowy_n.find("+KO");
	if (KOD6 != std::string::npos)
	{
		kod_pocztowy_n.erase(0, KOD6);
	}
	kod_pocztowy_n.erase(0, 4);
	size_t KOD7 = kod_pocztowy_n.find("+");
	if (KOD7 != std::string::npos)
	{
		kod_pocztowy_n.erase(KOD7);
	}
	kod_pocztowy_n.erase(6, 1);
	//cout << kod_pocztowy_n << endl;
}
void  ODCZYT_DANYCH::getCOM_BB()
{
	//NAZWA NABYWCY
	nazwa_odbiorcy = COM_BB;
	nazwa_odbiorcy.erase(0, 7);
	size_t KOD1 = nazwa_odbiorcy.find("+");
	if (KOD1 != std::string::npos)
	{
		nazwa_odbiorcy.erase(KOD1);
		//cout << nazwa_odbiorcy << endl;
	}
	//NIP
	nip_o = COM_BB;
	nip_o.erase(0, 7);
	size_t KOD2 = nip_o.find("+");
	if (KOD2 != std::string::npos)
	{
		nip_o.erase(0, KOD2);
	}
	nip_o.erase(0, 4);
	size_t KOD3 = nip_o.find("+");
	if (KOD3 != std::string::npos)
	{
		nip_o.erase(KOD3);
		//cout << nip_o << endl;
	}

	//MIASTO
	miasto_o = COM_BB;
	size_t KOD4 = miasto_o.find("+MT");
	if (KOD4 != std::string::npos)
	{
		miasto_o.erase(0, KOD4);
	}
	miasto_o.erase(0, 4);
	size_t KOD5 = miasto_o.find("+");
	if (KOD5 != std::string::npos)
	{
		miasto_o.erase(KOD5);
		//cout << miasto_o << endl;
	}

	//KOD POCZTOWY
	kod_pocztowy_o = COM_BB;
	kod_pocztowy_o.erase(0, 4);
	size_t KOD6 = kod_pocztowy_o.find("+KO");
	if (KOD6 != std::string::npos)
	{
		kod_pocztowy_o.erase(0, KOD6);
	}
	kod_pocztowy_o.erase(0, 4);
	size_t KOD7 = kod_pocztowy_o.find("+");
	if (KOD7 != std::string::npos)
	{
		kod_pocztowy_o.erase(KOD7);
	}
	kod_pocztowy_o.erase(6, 1);
	//cout << kod_pocztowy_o << endl;
}
void  ODCZYT_DANYCH::getDTM()
{
	//KOD WYSTAWIENIA
	KOD_DATY = DTM;
	KOD_DATY.erase(0, 4);
	size_t KOD1 = KOD_DATY.find("+");
	if (KOD1 != std::string::npos)
	{
		KOD_DATY.erase(KOD1);
	}
	if (KOD_DATY == "3")
	{
		KOD_DATY = "PLATNOSCI";
	}
	else if (KOD_DATY == "4")
	{
		KOD_DATY = "DOSTAWY";
	}
	else if (KOD_DATY == "137")
	{
		KOD_DATY = "WYSTAWIENIA FAKTURY";
	}
	
	//DATA WYSTAWIENIA DOKUMENTU
	data_wystawienia_dokumentu = DTM;
	data_wystawienia_dokumentu.erase(0, 4);
	size_t KOD2 = data_wystawienia_dokumentu.find("+");
	if (KOD2 != std::string::npos)
	{
		data_wystawienia_dokumentu.erase(0, KOD2);
		data_wystawienia_dokumentu.erase(0, 1);
	}
	size_t KOD3 = data_wystawienia_dokumentu.find("+");
	if (KOD3 != std::string::npos)
	{
		data_wystawienia_dokumentu.erase(KOD3);
	}
	//FORMAT DATY
	format = DTM;
	format.erase(0, 4);
	size_t KOD4 = format.find("+");
	if (KOD4 != std::string::npos)
	{
		format.erase(0, KOD2);
		format.erase(0, 1);
	}
	size_t KOD5 = format.find("+");
	if (KOD5 != std::string::npos)
	{
		format.erase(0, KOD5);
		format.erase(0, 1);
		format.erase(3, 1);
	}
	if (format == "102")
	{
		data_wystawienia_dokumentu.insert(4, "-");
		data_wystawienia_dokumentu.insert(7, "-");
		format = "RRRR-MM-DD";
	}
	else if (format == "103")
	{
		data_wystawienia_dokumentu.insert(2, "-");
		data_wystawienia_dokumentu.insert(5, "-");
		format = "DD-MM-RRRR";
	}
	else if (format != "102" && format != "103")
	{
		cout << "W segmencie DTM nie znaleziono KODU DOKUMENTU.\nBrak uruchomienia sekwencji nr 2 - Identyfikator czasu i daty.";
	}
}
void  ODCZYT_DANYCH::getCTA()
{
	//IMIE I NAZWISKO OSOBY WYSTAWIAJACEJ
	wystawiajacy = CTA;
	wystawiajacy.erase(0, 7);
	size_t KOD1 = wystawiajacy.find("'");
	if (KOD1 != std::string::npos)
	{
		wystawiajacy.erase(KOD1);
	}
}
void  ODCZYT_DANYCH::getSPRAWDZ_LIN()
{
	size_t KOD1 = LIN_6.find("LIN+6");
	if (KOD1 != std::string::npos)
	{
		pozycje_transportowe = 6;
	}
	else
	{
		size_t KOD2 = LIN_5.find("LIN+5");
		if (KOD2 != std::string::npos)
		{
			pozycje_transportowe = 5;
		}
		else
		{
			size_t KOD3 = LIN_4.find("LIN+4");
			if (KOD3 != std::string::npos)
			{
				pozycje_transportowe = 4;
			}
			else 
			{
				size_t KOD4 = LIN_3.find("LIN+3");
				if (KOD4 != std::string::npos)
				{
					pozycje_transportowe = 3;
				}
				else
				{
					size_t KOD5 = LIN_2.find("LIN+2");
					if (KOD5 != std::string::npos)
					{
						pozycje_transportowe = 2;
					}
					else
					{
						size_t KOD6 = LIN_1.find("LIN+1");
						if (KOD6 != std::string::npos)
						{
							pozycje_transportowe = 1;
						}
						else
						{
							cout << "W segmencie LIM nie znaleziono zadnej pozycji\nBrak uruchomienia sekwencji nr 4 - Pozycje towarowe.\n";
						}
					}
				}
			}
		}
	}
}
void  ODCZYT_DANYCH::getLIN()
{
	//NUMER POZYCJI
	for (int i = 1; i <= pozycje_transportowe; i++)
	{
		if (i == 1)
		{
			nr_pozycji = LIN_1;
			nazwa_towaru = LIN_1;
			ilosc = LIN_1;
			cena_jednostkowa = LIN_1;
			cena_razem = LIN_1;
		}
		else if (i == 2)
		{
			nr_pozycji = LIN_2;
			nazwa_towaru = LIN_2;
			ilosc = LIN_2;
			cena_jednostkowa = LIN_2;
			cena_razem = LIN_2;
		}
		else if (i == 3)
		{
			nr_pozycji = LIN_3;
			nazwa_towaru = LIN_3;
			ilosc = LIN_3;
			cena_jednostkowa = LIN_3;
			cena_razem = LIN_3;
		}
		else if (i == 4)
		{
			nr_pozycji = LIN_4;
			nazwa_towaru = LIN_4;
			ilosc = LIN_4;
			cena_jednostkowa = LIN_4;
			cena_razem = LIN_4;
		}
		else if (i == 5)
		{
			nr_pozycji = LIN_5;
			nazwa_towaru = LIN_5;
			ilosc = LIN_5;
			cena_jednostkowa = LIN_2;
			cena_razem = LIN_2;
		}
		else if (i == 6)
		{
			nr_pozycji = LIN_6;
			nazwa_towaru = LIN_6;
			ilosc = LIN_6;
			cena_jednostkowa = LIN_6;
			cena_razem = LIN_6;
		}
		
		//NUMER POZYCJI TRANSPORTOWEJ
		nr_pozycji.erase(0, 4);
		size_t KOD1 = nr_pozycji.find("+");
		if (KOD1 != std::string::npos)
		{
			nr_pozycji.erase(KOD1);
		}

		//NAZWA TOWARU
		nazwa_towaru.erase(0, 9);
		size_t KOD2 = nazwa_towaru.find("+");
		if (KOD2 != std::string::npos)
		{
			nazwa_towaru.erase(KOD2);
		}
		//ILOSC
		ilosc.erase(0, 9);
		size_t KOD3 = ilosc.find(":");
		if (KOD3 != std::string::npos)
		{
			ilosc.erase(0, KOD3);
			ilosc.erase(0, 1);
		}
		size_t KOD4 = ilosc.find("+");
		if (KOD4 != std::string::npos)
		{
			ilosc.erase(KOD4);
		}
		//CENA JEDNOSTKOWA
		cena_jednostkowa.erase(0, 9);
		size_t KOD5 = cena_jednostkowa.find("PR:");
		if (KOD5 != std::string::npos)
		{
			cena_jednostkowa.erase(0, KOD5);
			cena_jednostkowa.erase(0, 3);
		}
		size_t KOD6 = cena_jednostkowa.find("+");
		if (KOD6 != std::string::npos)
		{
			cena_jednostkowa.erase(KOD6);
		}
		//CENA RAZEM
		cena_razem.erase(0, 9);
		size_t KOD7 = cena_razem.find("RPA:");
		if (KOD7 != std::string::npos)
		{
			cena_razem.erase(0, KOD7);
			cena_razem.erase(0, 4);
		}
		size_t KOD8 = cena_razem.find("'");
		if (KOD8 != std::string::npos)
		{
			cena_razem.erase(KOD8);
		}
		cout << nr_pozycji << "\t" " \t" << nazwa_towaru << "\t"" \t" << ilosc << "\t""\t" << cena_jednostkowa << "\t""\t""\t" << cena_razem << endl;
	}
}
void  ODCZYT_DANYCH::getKOMUNIKAT_KONCOWY()
{
	cout << "Raport zawiera nastepujace segmenty:" << endl;
	
	cout << "1. POCZATEK KOMUNIKATU: \n";
	cout << "Kod dokumentu: " << kod_dokumentu << "\n";
	cout << "Numer referenyjny: " << NR_REF << "\n";
	cout << "Wersja dokumentu: " << wersja_dokumentu << "\n" << endl;

	cout << "2. IDENTYFIKACJA DATY/CZASU: \n";
	cout << "Data: " << KOD_DATY << "\n";
	cout << "Format daty: " << format << "\n";
	cout << "Data: " << data_wystawienia_dokumentu << "\n" << endl;

	cout << "3. IDENTYFIKACJA NABYWCY I DOSTAWCY: \n";
	cout << "Nabywca: " << endl;
	cout << "Nazwa nabywcy: " << nazwa_nabywcy << "\n";
	cout << "NIP nabywcy: " << nip_n << "\n";
	cout << "Miasto nabywcy: " << miasto_n << "\n";
	cout << "Kod pocztowy nabywcy: " << kod_pocztowy_n << "\n" << endl;

	cout << "Odbiorca: " << endl;
	cout << "Nazwa odbiorcy: " << nazwa_odbiorcy << "\n";
	cout << "NIP odbiorcy: " << nip_o << "\n";
	cout << "Miasto odbiorcy: " << miasto_o << "\n";
	cout << "Kod pocztowy odbiorcy: " << kod_pocztowy_o << "\n" << endl;

	cout << "4. POZYCJE TOWAROWE: \n";
	cout << "Nr pozycji:\tNazwa towaru:\tIlosc:\t\tCena jednostkowa\tCena razem" << endl;
	ODCZYT_DANYCH::getLIN();

	cout << endl;
	cout << "5. OSOBA WYSTAWIAJACA DOKUMENT: \n";
	cout<< "Imie i nazwisko: " <<wystawiajacy << "\n" << endl;
	
	cout << "6. PLATNOSC: \n";
	cout << "Sposob platnosci: " << sposob_platnosci << "\n";
	cout << "Kwota: " << kwota << "\n";
	cout << "Waluta: " << waluta << "\n" << endl;

}